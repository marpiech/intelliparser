class IndexedFile
	require 'set'
	attr_accessor :name,:cage,:starttime,:endtime,:mice,:experiment
	def initialize
		self.mice = []
	end
end

class FilesController < ApplicationController
	require 'zip'
	def index
		@indexedFiles = Array.new
  		@files = Dir.glob('test/resources/*.zip')
  		for file in @files
    		puts file # printing file
    		unzip(file)
		end
  	end

	def unzip(file)

		puts "unzipping"
		begin
			Zip::File.open(file) do |zipfile|
				indexedFile = IndexedFile.new
				indexedFile.name = zipfile.name
				parseVisits(zipfile.read("IntelliCage/Visits.txt"), indexedFile)
				zipfile.each do |file|
					if Regexp.new("Experiments/").match(file.name)
						indexedFile.experiment = Regexp.new('([^/]+)$').match(file.name)
					end
				end

				@indexedFiles.push(indexedFile)

			end
		rescue Exception => e
			puts e.message  
		end
	end

	def parseVisits(text, indexedFile)
		lines = text.split(/\n/)
		elements = lines[1].split(/\t/)
		indexedFile.cage = elements[5]
		indexedFile.starttime = elements[2]
		elements = lines[-1].split(/\t/)
		indexedFile.endtime = elements[2]
		lines.each do |line|
			mouse = line.split(/\t/)[1]
			if (!indexedFile.mice.include? mouse)
				indexedFile.mice.push(mouse)
			end
		end
		indexedFile.mice.shift
		indexedFile.mice = indexedFile.mice.sort
	end

	def show
		merge = params[:files]
  		send_data generate(merge), filename: 'nosepokes.txt'
	end

	def generate(merge)
		selectedFiles = merge.split(",")
		header = "VisitID	Start	End	Side	SideCondition	SideError	TimeError	ConditionError	LickNumber	LickContactTime	LickDuration	AirState	DoorState	LED1State	LED2State	LED3State	AnimalTag"
		text = header + "\n"
		selectedFiles.each do |selectedFile|
			header = ""
			nosepokes = []
			visits = []
			Zip::File.open(selectedFile) do |zipfile|
				visits = zipfile.read("IntelliCage/Visits.txt").lines
				visits.shift
				nosepokes = zipfile.read("IntelliCage/Nosepokes.txt").lines
				nosepokes.shift
			end
			lastVisit = ""
			visitCounter = 0;
			animalTag = ""
			nosepokes.each do |nosepoke|
				puts "====== START ======"
				visit = nosepoke.split("\t")[0]
				puts nosepoke.split("\t").length
				puts (lastVisit != visit)
				puts "VISIT"
				puts lastVisit + " " + visit
				if (lastVisit != visit)
					lastVisit = visit
					puts "=========="
					puts nosepoke
					puts visit
					puts visits[visitCounter - 1]
					animalTag = visits[visitCounter].split("\t")[1]
					visitCounter = visitCounter + 1
				end
				text = text + nosepoke.strip + "\t" + animalTag + "\n"
			end
		end
		return text
	end

end

